// 1. Find web developers

const employeeData = require("../data");

const findWebDevelpers = (arr) => {
  let webDevelopers = arr.filter((eachEmployee, index, arr) => {
    if (eachEmployee["job"].includes("Web Developer")) return eachEmployee;
  });
  return webDevelopers;
};

//console.log(findWebDevelpers(employeeData))

// 2. Convert to strings

const convertToStrings = (arr) => {
  employee = arr.forEach((eachEmployee) => {
    eachEmployee["salary"] = parseFloat(eachEmployee["salary"].slice(1));
  });

  return employee;
};

//3. Corrected Salaries

const corectSalaries = (arr) => {
  arr.map((eachEmployee, index, arr) => {
    eachEmployee["corrected_salary"] = eachEmployee["salary"] * 10000;
  });
};

// 4.Sum of salaries

const sumOfSalaries = (arr) => {
  let totalSalary = arr.reduce((sum, eachEmployee, index, arr) => {
    
    return sum + eachEmployee["salary"];
  }, 0);

  return totalSalary;
};

//console.log(sumOfSalaries(employeeData))

// 5.Sum of salaries based on country ..

const sumOfSalariesForEachCountry = (arr) => {
  // arr.map((eachEmployee, index, arr) => {
  //   if (salariesForEachCountry[eachEmployee["location"]]) {
  //     salariesForEachCountry[eachEmployee["location"]]["sum_of_salary"] +=
  //       eachEmployee["salary"];
  //   } else {
  //     salariesForEachCountry[eachEmployee["location"]] = {};
  //     salariesForEachCountry[eachEmployee["location"]]["sum_of_salary"] =
  //       eachEmployee["salary"];
  //   }
  // });

  let salariesForEachCountry = arr.reduce((result, eachEmployee) => {
    if (result[eachEmployee["location"]]) {
      result[eachEmployee["location"]]["sum_of_salary"] +=
        eachEmployee["salary"];
    } else {
      result[eachEmployee["location"]] = {};

      result[eachEmployee["location"]] = {
        sum_of_salary: (result[eachEmployee["location"]]["sum_of_salary"] =
          eachEmployee["salary"]),
      };
    }
    return result;
  }, {});

  return salariesForEachCountry;
};

//console.log(sumOfSalariesForEachCountry(employeeData))

//6. Average salary based on country

const avgSalaryBasedForEachCountry = (arr) => {
  let salariesForEachCountry = arr.reduce(
    (result, eachEmployee, index, arr) => {
      if (result[eachEmployee["location"]]) {
        result[eachEmployee["location"]]["sum_of_salary"] +=
          eachEmployee["salary"];

        result[eachEmployee["location"]]["employees_count"] += 1;
      } else {
        result[eachEmployee["location"]] = {};

        result[eachEmployee["location"]]["sum_of_salary"] =
          eachEmployee["salary"];
        result[eachEmployee["location"]]["employees_count"] = 1;

        //result[eachEmployee["location"]]["total_employees"] = 1;
      }
      return result;
    },
    {}
  );

  let averageOfEachcountry = {};

  Object.keys(salariesForEachCountry).map((eachCountry) => {
    averageOfEachcountry[eachCountry] =
      salariesForEachCountry[eachCountry]["sum_of_salary"] /
      salariesForEachCountry[eachCountry]["employees_count"];
  });

  return averageOfEachcountry;
};

//console.log(avgSalaryBasedForEachCountry(employeeData))

module.exports = {
  findWebDevelpers,
  convertToStrings,
  corectSalaries,
  sumOfSalaries,
  sumOfSalariesForEachCountry,
  avgSalaryBasedForEachCountry,
};
